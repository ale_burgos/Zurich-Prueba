package com.mx.intellego.zurich.ecm;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFolder;

public class Carpeta {

	public  BoxFolder.Info createFolder(String tokenConnection, String id2) {

		System.out.println("Crea una nueva Carpeta");

		BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
		BoxFolder parentFolder = new BoxFolder(api, id2);
		BoxFolder.Info childFolderInfo = parentFolder.createFolder("CarpetaZurich");
		return childFolderInfo;
		
	}
}
