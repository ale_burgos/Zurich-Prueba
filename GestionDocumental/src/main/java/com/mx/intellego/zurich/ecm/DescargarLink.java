package com.mx.intellego.zurich.ecm;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;

public class DescargarLink {

public void getDownloadURL(String tokenConnection, String idDocumento) throws IOException{
		
		OutputStream output;
		System.out.println("Descargar Link de Ducumento");
		
		BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
		BoxFile file = new BoxFile(api, idDocumento);
		URL info = file.getDownloadURL();
		
		System.out.println(file.getDownloadURL());
		System.out.println("Se descargo el Archivo");
	}
}
