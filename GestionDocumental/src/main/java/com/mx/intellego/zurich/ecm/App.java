package com.mx.intellego.zurich.ecm;

import java.io.IOException;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;

/**
 * Hello world!
 *
 */
public class App 
{
	public BoxAPIConnection conexion(String token) {
		BoxAPIConnection api = new BoxAPIConnection(token);
		return api;
	}

	public static void main(String[] args) throws IOException

	{
		String tokenConnection = "8l4KLMKwDmLav0QvVt8ug1i8b4MlIEYO";
		String sourceFile = "C:/Hola.pdf";
		String destFile = "talula.pdf";
		String destFile2 = "Ale2.pdf";
		String id = "94416891457";
		String id2 = "11312159536";
		String idDocumento = "94939948694";
		String file = "Ale";
		try {

			{

				System.out.println("API BOX");

				App a = new App();
				BoxAPIConnection api = a.conexion(tokenConnection);

				BoxFolder rootFolder = BoxFolder.getRootFolder(api);
				for (BoxItem.Info itemInfo : rootFolder) {
					System.out.format("[%s] %s\n", itemInfo.getID(), itemInfo.getName());
				}

				// Crear Carpeta
				// Carpeta carpet = new Carpeta();

				try {
					Carpeta carpet = new Carpeta();

					carpet.createFolder(tokenConnection, id2);

				} catch (Exception e) {
					// El Metodo crear carpeta fallo
					e.printStackTrace();
				}

				// Subir Documento

				// objeto archivo

				try {

					CanUpload subir = new CanUpload();

					subir.subir(tokenConnection, sourceFile, destFile);
				} catch (Exception e) {

					e.printStackTrace();
				}

				// Descargar Documento

				try {

					Descargar descargar = new Descargar();

					descargar.OutputStreamdownload(tokenConnection, idDocumento);

				} catch (Exception e) {

					e.printStackTrace();
				}

				// Meta Datos
				try {

					MetaDato MetaDato = new MetaDato();

					MetaDato.createMetadata(tokenConnection, id);

					MetaDato.updateMetadata(tokenConnection, id);

					MetaDato.getMetadata(tokenConnection, id);

				} catch (Exception e) {

					e.printStackTrace();
				}

				// Versionar Documento

				try {

					Versionar Versionar = new Versionar();

					Versionar.previoFilegetVersions(tokenConnection, id, destFile2, sourceFile);

					Versionar.uploadVersion(tokenConnection, id, destFile2, sourceFile);

				} catch (Exception e) {

					e.printStackTrace();

				}

				// carpet.createFolder(tokenConnection, id2);

				// Generar Descarga de Link

				try {

					DescargarLink DescargarLink = new DescargarLink();

					DescargarLink.getDownloadURL(tokenConnection, idDocumento);

				} catch (Exception e) {

					e.printStackTrace();

				}

				try {

					Previsual Previsual = new Previsual();

					Previsual.viewUrlbox(tokenConnection, idDocumento);

				} catch (Exception e) {

					e.printStackTrace();
				}

			}

		} catch (Exception e)

		{
			e.printStackTrace();
		}

	}
}
