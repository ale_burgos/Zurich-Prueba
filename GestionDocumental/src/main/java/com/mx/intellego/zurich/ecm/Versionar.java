package com.mx.intellego.zurich.ecm;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;

public class Versionar {

	// Previo Doc

		public void previoFilegetVersions(String tokenConnection, String sourceFile, String destFile2, String id) {

			BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
			BoxFile file = new BoxFile(api, id);
			FileInputStream stream = null;
			try {
				stream = new FileInputStream(sourceFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			file.uploadVersion(stream);

		}

		// Nueva Version

		public void uploadVersion(String tokenConnection, String sourceFile, String destFile2, String id)
				throws FileNotFoundException {

			BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
			BoxFile file = new BoxFile(api, id);
			FileInputStream stream = new FileInputStream(id);
			file.uploadVersion(stream);

		}
}
